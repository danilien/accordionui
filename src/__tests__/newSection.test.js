import { mockedWithModalInnerHTML } from './data/mockedData';
import { addOnSubmitNewSection } from '../js/lib/common';
import {
    getSectionTitleInput,
    getSectionContentInput,
    getAccordionList,
    getAccordion,
    getModal,
    getForm,
    createNewElement
} from '../js/lib/domAccess';

describe('newSection interaction tests', () => {
    beforeAll(() => {
        document.body.innerHTML = mockedWithModalInnerHTML;
        addOnSubmitNewSection(
            getForm(),
            getSectionTitleInput(),
            getSectionContentInput(),
            getAccordionList(),
            getAccordion(),
            createNewElement,
            getModal()
        );
    });
    it('click onsubmit with invalid new section', () => {
        const event = new Event('submit');
        const initialLength = getAccordionList().length;
        getForm().dispatchEvent(event);
        expect(getAccordionList().length).toBe(initialLength);
    });
    it('click onsubmit with valid section', () => {
        const event = new Event('submit');
        const initialLength = getAccordionList().length;
        getSectionTitleInput().value = 'Some new section';
        getSectionContentInput().value = 'Some new content';
        getForm().dispatchEvent(event);
        expect(getAccordionList().length).toBe(initialLength + 1);
    });
});