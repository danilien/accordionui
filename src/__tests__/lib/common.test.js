import { createNewSection, onClickSection } from '../../js/lib/common';
import { mockedSimpleInnerHTML } from '../data/mockedData';
import { getAccordionList } from '../../js/lib/domAccess';

const mockedCreateElement = () => ({
    classList: {
        add: () => { }
    },
    appendChild: () => { },
    addEventListener: () => { }
});

describe('common tests', () => {
    beforeAll(() => {
        document.body.innerHTML = mockedSimpleInnerHTML;
    });
    it('createNewSection test', () => {
        const {
            titleElement,
            panelElement
        } = createNewSection('Section 4', 'Section content', getAccordionList(), mockedCreateElement);
        expect(titleElement.innerHTML).toBe('Section 4');
        expect(panelElement).toBeDefined();
    });
    it('onClickSection test', () => {
        const firstSection = getAccordionList().item(0);
        onClickSection(firstSection, getAccordionList())();
        getAccordionList().forEach((element) => {
            if (element === firstSection) {
                expect(element.classList).toContain('active');
            } else {
                expect(element.classList).not.toContain('active');
            }
        });
    });
});