export const mockedSimpleInnerHTML = `<dl id="accordionContainerId" class="accordionContainer">
    <dt class="accordion" > Section 1</dt >
        <dd class="panel">
            <p>Section 1 Content...</p>
        </dd>
        <dt class="accordion">Section 2</dt>
        <dd class="panel">
            <p>Section 2 Content...</p>
        </dd>
        <dt class="accordion">Section 3</dt>
        <dd class="panel">
            <p>Section 3 Content...</p>
        </dd>
    </dl >`;

export const mockedWithModalInnerHTML = `<div>
    <dl id="accordionContainerId" class="accordionContainer">
        <dt class="accordion">Section 1</dt>
        <dd class="panel">
            <p>Section 1 Content...</p>
        </dd>
        <dt class="accordion">Section 2</dt>
        <dd class="panel">
            <p>Section 2 Content...</p>
        </dd>
        <dt class="accordion">Section 3</dt>
        <dd class="panel">
            <p>Section 3 Content...</p>
        </dd>
    </dl>

    <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>Add new section</h2>
            </div>
            <div class="modal-body">
                <form id="my-form" class="form">
                    <div class="field">
                        <!-- <span class="title">Title:</span> -->
                        <!-- <input type="text" id="sectionInputTitle"> -->
                        <input type="text" id="sectionInputTitle" placeholder="Section name...">
                    </div>
                    <div class="field">
                        <!-- <span class="title">Content:</span> -->
                        <input type="text" id="sectionInputContent" placeholder="Section content...">
                    </div>
                    <input class="submitNewSection" type="submit" value="Submit">
                </form>
            </div>
        </div>
    </div>
</div>`;