import { addClickSectionListeners } from '../js/lib/common';
import { getAccordionList } from '../js/lib/domAccess';
import { mockedSimpleInnerHTML } from './data/mockedData';

describe('main interaction tests', () => {
    beforeAll(() => {
        document.body.innerHTML = mockedSimpleInnerHTML;
        addClickSectionListeners(getAccordionList());
    });
    it('click on first section', () => {
        const event = new Event('click');
        const firstSection = getAccordionList().item(0);
        firstSection.dispatchEvent(event);
        getAccordionList().forEach((element) => {
            if (element === firstSection) {
                expect(element.classList).toContain('active');
            } else {
                expect(element.classList).not.toContain('active');
            }
        });
    });
    it('click on first section and second section', () => {
        const event = new Event('click');
        const firstSection = getAccordionList().item(0);
        firstSection.dispatchEvent(event);
        const secondSection = getAccordionList().item(0);
        secondSection.dispatchEvent(event);
        getAccordionList().forEach((element) => {
            if (element === secondSection) {
                expect(element.classList).toContain('active');
            } else {
                expect(element.classList).not.toContain('active');
            }
        });
    });
});