import '../css/main.scss';
import './newSection';
import './modal';
import { addClickSectionListeners } from './lib/common';
import { getAccordionList } from './lib/domAccess';

addClickSectionListeners(getAccordionList());