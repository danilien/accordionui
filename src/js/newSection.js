import '../css/newSection.scss';
import {
    getSectionTitleInput,
    getSectionContentInput,
    getAccordionList,
    getAccordion,
    getModal,
    getForm,
    createNewElement
} from './lib/domAccess';
import { addOnSubmitNewSection } from './lib/common';

addOnSubmitNewSection(
    getForm(),
    getSectionTitleInput(),
    getSectionContentInput(),
    getAccordionList(),
    getAccordion(),
    createNewElement,
    getModal()
);