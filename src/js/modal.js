import '../css/modal.scss';
import {
    getClose,
    getOpenButton,
    getModal
} from './lib/domAccess';

// When the user clicks the button, open the modal
getOpenButton().addEventListener('click', () => {
    getModal().style.display = 'block';
});

getClose().onclick = () => {
    getModal().style.display = 'none';
};

window.onclick = (event) => {
    if (event.target === getModal) {
        getModal().style.display = 'none';
    }
};