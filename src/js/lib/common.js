/* eslint-disable no-param-reassign */

const switchElementStatus = (element, isActive) => {
    /* Remove or add the 'active' class bassed on the active status */
    if (isActive) {
        element.classList.add('active');
    } else {
        element.classList.remove('active');
    }
    /* Hide or display the content bassed on the active status  */
    // eslint-disable-next-line no-param-reassign
    element.nextElementSibling.style.display = isActive ? 'block' : 'none';
};

export const onClickSection = (element, acordionList) => () => {
    /* Retrieve current element status */
    const activeStatus = element.classList.contains('active');
    Array.from(acordionList).forEach((secElement) => {
        switchElementStatus(secElement, false);
    });
    switchElementStatus(element, !activeStatus);
};

export const addClickSectionListeners = (accordionList) => {
    Array.from(accordionList).forEach((element) => {
        element.addEventListener('click', onClickSection(element, accordionList));
    });
};

export const createNewSection = (title, content, accordionList, createNewElement) => {
    const titleElement = createNewElement('dt');
    titleElement.innerHTML = title;
    titleElement.classList.add('accordion');
    titleElement.addEventListener('click', onClickSection(titleElement, accordionList));
    const contentElement = createNewElement('p');
    contentElement.innerHTML = content;
    const panelElement = createNewElement('dd');
    panelElement.classList.add('panel');
    panelElement.appendChild(contentElement);
    return {
        titleElement,
        panelElement
    };
};

const processForm = (
    sectionTitleInput,
    sectionContentInput,
    accordionList,
    accordion,
    createNewElement,
    modal
) => (event) => {
    if (event.preventDefault) event.preventDefault();
    const title = sectionTitleInput.value.trim();
    const content = sectionContentInput.value.trim();
    if (title && content) {
        const {
            titleElement,
            panelElement
        } = createNewSection(title, content, accordionList, createNewElement);
        accordion.appendChild(titleElement);
        accordion.appendChild(panelElement);
        // Close modal
        modal.style.display = 'none';
        // Clean form
        sectionTitleInput.value = '';
        sectionContentInput.value = '';
    } else {
        // If any of the input fields is empty display some error
    }
    return false;
};

export const addOnSubmitNewSection = (
    form,
    sectionTitleInput,
    sectionContentInput,
    accordionList,
    accordion,
    createNewElement,
    modal
) => {
    form.addEventListener('submit',
        processForm(
            sectionTitleInput,
            sectionContentInput,
            accordionList,
            accordion,
            createNewElement,
            modal
        ));
};
